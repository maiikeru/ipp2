#!/usr/bin/env python
# -*- coding: utf-8 -*-
#CST:xjurca07

##
# Project: IPP CST (C Stats), VUT FIT
# Author: Michal Jurca, xjurca07@stud.fit.vutbr.cz
# Date: 7.4.2014
#
# File: cst.py
# Version: 0.8
# Change: 16.4.2014
##

#Library
import sys
import re
import os
import codecs

#Global variable
g_input = ''
g_output = ''
g_pattern = ''
g_arrayFile = []
g_arrayStats = []
g_param = ''
g_p = 0
g_s = 0


def print_help():
    """
    Tisknuti napovedy.
    """
    print("Projekt: CST (C Stats) v Python3, projekt c2 IPP")
    print("################################################")
    print("Autor: Michal Jurca, xjurca07@stud.fit.vutbr.cz")
    print("Popis: Scrirpt analyzuje zdrojove soubory jazyka C podle standardu ISO C99,")
    print("       ktery ve stanovenem foramtu vypisuje statistiky komentaru, klicovych slov")
    print("       operatoru, retezcu")
    print("       Parametry -k, -o, -i, -w, -c nelze mezi sebou kombinovat")
    print("Paremetry")
    print("  --help\t\t* tiskne napovedu")
    print("  --input=fileordir\t* vstupni soubor nebo adresar")
    print("  --nosubdir\t\t* prohledavani v zadanem adresari")
    print("  --output=filename\t* vystupni soubor")
    print("  -k\t\t\t* vypise pocet klicovych slov")
    print("  -o\t\t\t* vypise pocet jednoduchych operatoru")
    print("  -i\t\t\t* vypise pocet identifikatoru")
    print("  -w=pattern\t\t* vraci pocet naleyeneho retezce pattern")
    print("  -c\t\t\t* vzpise pocet komentaru")
    print("  -p\t\t\t* vypis bude tisknut bez absolutni cesty")


#end printHelp

def param():
    """
    Zpracovani vstupnich parametru. Metoda pristupuje ke globalnim promenym. Pro uloyeni vstupu,vystupu,
    hledaneho reteyce a bool hodnata pro paramtr p
    """

    global g_input, g_output, g_pattern, g_param, g_p, g_s
    prep_b = 0

    subdir = 0
    vstup = 0
    vystup = 0
    chyba = 0
    preph = 0
    total = len(sys.argv)

    err_message_1 = "Unknow parameter! Try '%s --help' for more information.\n" % sys.argv[0]
    err_message_2 = "Specified multiple times --input. Try '%s --help' for more information.\n" % sys.argv[0]
    err_message_3 = "Can not be combined --input and --nosubdir. Try '%s --help' for more information.\n" % sys.argv[0]
    err_message_4 = "Specified multiple times --nosubdir. Try '%s --help' for more information.\n" % sys.argv[0]
    err_message_5 = "Specified multiple times --output. Try '%s --help' for more information.\n" % sys.argv[0]
    err_message_6 = "Can not be combined -c,-k,-i,-o,-w. Try '%s --help' for more information.\n" % sys.argv[0]
    err_message_7 = "Specified multiple times -p. Try '%s --help' for more information.\n" % sys.argv[0]
    err_message_8 = "Unspecified parameter -c,-k,-i,-o,-w. Try '%s --help' for more information.\n" % sys.argv[0]
    err_message_9 = "Unspecified value of pattern. Try '%s --help' for more information.\n" % sys.argv[0]
    err_message_10 = "Specified multiple times -p. Try '%s --help' for more information.\n" % sys.argv[0]

    if total > 7 or total == 1:
        sys.stderr.write(err_message_1)
        sys.exit(1)
    else:
        for o in sys.argv[1:]:
            if o == '--help':
                preph = 1
            elif re.match(r"^--input=.+", o):
                if vstup == 1:
                    sys.stderr.write(err_message_2)
                    sys.exit(1)
                else:
                    g_input = o[8:]
                    vstup = 1
                    if os.path.isfile(g_input) and subdir == 1:
                        sys.stderr.write(err_message_3)
                        sys.exit(1)
            elif o == '--nosubdir':
                if subdir == 1:
                    sys.stderr.write(err_message_4)
                    sys.exit(1)
                elif vstup == 1:
                    if os.path.isfile(g_input):
                        sys.stderr.write(err_message_3)
                        sys.exit(1)
                else:
                    subdir = 1
            elif re.match(r"^--output=.+", o):
                if vystup == 1:
                    sys.stderr.write(err_message_5)
                    sys.exit(1)
                else:
                    g_output = o[9:]
                    vystup = 1
            elif o == '-k':
                if prep_b == 1:
                    sys.stderr.write(err_message_6)
                    sys.exit(1)
                else:
                    prep_b = 1
                    g_param = 'k'
            elif o == '-o':
                if prep_b == 1:
                    sys.stderr.write(err_message_6)
                    sys.exit(1)
                else:
                    prep_b = 1
                    g_param = 'o'
            elif o == '-i':
                if prep_b == 1:
                    sys.stderr.write(err_message_6)
                    sys.exit(1)
                else:
                    prep_b = 1
                    g_param = 'i'
            elif re.match(r"^-w=.*", o):
                if prep_b == 1:
                    sys.stderr.write(err_message_6)
                    sys.exit(1)
                else:
                    g_pattern = o[3:]
                    prep_b = 1
                    g_param = 'w'
            elif o == '-c':
                if prep_b == 1:
                    sys.stderr.write(err_message_6)
                    sys.exit(1)
                else:
                    prep_b = 1
                    g_param = 'c'
            elif o == '-p':
                if g_p == 1:
                    sys.stderr.write(err_message_7)
                    sys.exit(1)

                g_p = 1
            elif o == '-s':
                if g_s == 1:
                    sys.stderr.write(err_message_10)
                    sys.exit(1)

                g_s = 1
            else:
                chyba = 11

    if chyba == 11:
        sys.stderr.write(err_message_1)
        sys.exit(1)

    if total == 2 and preph == 1:
        print_help()
        sys.exit(0)

    if prep_b == 0:
        sys.stderr.write(err_message_8)
        sys.exit(1)

    if preph == 1 and prep_b == 1 and total == 3:
        sys.stderr.write(err_message_1)
        sys.exit(1)

    if g_param == 'w':
        if g_pattern == "":
            sys.stderr.write(err_message_9)
            sys.exit(155)


#end param()

def analyze_input():
    """
    Metoda pristupuje do globalni promenne g_input, kde je ulozen  soubor nebo adresar.
    Bude-li na vstupu soubor tak se ulozi do globani promenne g_arrayFile. Bude-li na vstupu adresar, tak
    je funkce vola metodu walkdir(). Nebudeli zadan parametr --input tak se bude prochazet aktualni
    adresar.
    """
    global g_input, g_arrayFile
    if os.path.isfile(g_input):
        g_arrayFile.append(g_input)
    elif os.path.isdir(g_input):
        walkdir(g_input)
    elif g_input == "":
        walkdir(u".")
    else:
        sys.stderr.write("Err input: non-existent entry")
        sys.exit(2)


#end analyzeInput()

def walkdir(dirname):
    """
    Metoda prochazi adresar hleda soubory jazyka C. Nalezeny soubot je ukladan do globalni prommene
    g_arrayFile

    @param dirname adresaar, krety se bude prochazet
    """
    global g_arrayFile

    for cur, dirs, files in os.walk(dirname):
        for soubor in files:
            if re.search(r'\.[chCH]$', soubor):
                patch = os.path.join(cur, soubor)
                g_arrayFile.append(patch)


# end walkdir()

def delete_backslash(source):
    """
    Metoda odstranuje \\

    @param source vstupni sobor
    @return text vystupni soubor, po odmazani \\
    """
    state = 0
    text = ""
    for b in source:
        if state == 0:
            if b == '\\':
                state = 1
            else:
                text += b
        elif state == 1:
            if b == '\n':
                state = 0
                text += ''

    return text


# end delete_backslash()

def delete_string(source):
    """
    Metoda odstanuje ze retezce a nahrazuje je ""
    @param source vstupni soubor
    @return text vystupni soub po odmazani rtezcu
    """
    state = 0
    text = ""
    for string in source:
        if state == 0:
            if string == '\"' or string == '\'':
                state = 1
                text += "\""
            else:
                text += string
        elif state == 1:
            if string == '\"' or string == '\'':
                state = 0
                text += "\""

    return text


# end delete_string()

def delete_macro2(source):
    """
    Metoda pomoci regulernich vyrazu maze makra a nahrazuje je prazdnym radkem
    @param source vstupni soubor
    @return source vystupni soubor po odstaneni maker
    """
    source = re.sub(r"#.*(\\*.*)*\n", r"\n", source)




    return source


# end delete_macro2()

def search_comment(source):
    """
    Metoda pomoci automatu spocita pocet komentaru a zaroven vraci upraveny soubor bez komentaru.

    @param source vstupni soubor
    @return src vystupni soubor, ktery neobssahuje komentare
    @return len(comment) vraci pocet komentaru
    """
    state = 0
    save = False
    comment = ""
    src = ""
    ch = ""
    text = ""

    i = 0
    while i < len(source):
        char = source[i]
        i += 1

        if state == 0:
            if save:
                comment += ch
                ch = ""
            else:
                ch = ""

            src += text
            text = ""

            if char == '/':
                state = 1
                ch += char
            elif char == '\"':
                state = 10
                text += char
            else:
                text += char

        elif state == 1:
            if char == '/':
                state = 2
                ch += char
            elif char == '*':
                state = 3
                ch += char
            elif char == '\\':
                state = 5
                ch += char
            else:
                state = 0
                text += ch[:(len(ch))]
                text += char
                ch = ch[:-1]
                save = False

        elif state == 2:
            if char == '\n' or i == (len(source)):
                state = 0
                ch += char
                save = True
            elif char == '\\':
                state = 22
                ch += char
            else:
                ch += char

        elif state == 22:
            if char == '\n':
                state = 2
                ch += char
            elif char == ' ' or char == '\t':
                state = 22
                ch += char
            else:
                state = 2
                ch += char

        elif state == 3:
            if char == '*':
                state = 4
                ch += char
            else:
                ch += char

        elif state == 4:
            if char == '*':
                ch += char
            elif char == '/':
                state = 0
                ch += char
                save = True
            elif char == '\\':
                state = 44
                ch += char
            else:
                ch += char

        elif state == 44:
            if char == '\n':
                state = 3
                ch += char
            else:
                ch += char

        elif state == 5:
            if char == '\n':
                state = 1
                ch += char
            elif char == ' ' or char == '\t':
                ch += char
            else:
                state = 0
                save = False
                text += char

        elif state == 10:
            if char == '\"':
                state = 0
                text += char
                save = False
            else:
                text += char
        else:
            text += char

    if save:
        comment += ch
    else:
        src += ch

    return len(comment), src


#end search_comment()


def search_keyword(source, keyword):
    """
    Metoda vraci pocet klicovych slov. Vstupni soubor je rozsekan pomoci regurelnich vyrazu.
    A pak se kazde slovo porovnava jestli to neni klicove slovo a inkementuje se pocitadlo.
    @param source vstupni soubor
    @param keyword klicova slova, ktere budou hledany
    @return count vraci  pocet klicovych slov
    """

    source = source.split("\n")
    text = ""
    for row in source:
        row = re.sub(r" ", r"\n", row)
        row = re.sub(r"\(", r" ", row)
        row = re.sub(r"\)", r" ", row)
        row = re.sub(r",", r" ", row)
        row = re.sub(r"\*", r" ", row)
        row = re.sub(r"\{", r" ", row)
        row = re.sub(r"\}", r" ", row)
        row = re.sub(r";", r" ", row)
        row = re.sub(r"\[", r" ", row)
        row = re.sub(r"]", r" ", row)
        text += row + "\n"

    count = 0
    for w in text.split():
        for key in keyword:
            if w == key:
                count += 1

    return count


#end search_keyword()

def search_identificator(source, keyword):
    """
    Metoda vraci pocet indentifikatoru. Vstupni soubor je rozsekan pomoci regulernich vyrazu na elementy,
    ktere jsou pak kontrolovany, jestli nejsou klicova slova, jestli ano tak se nahradi mezerou. Zbytek textu
    je jeste analyzovan jestli zacina pismenen.

     @param source vstupni soubor
     @param keyword klicova slova, ktere budou vynechany
     @return count vraci pocet identifikatoru
    """
    source = source.split("\n")
    text = ""
    for row in source:
        row = re.sub(r" ", r"\n", row)
        row = re.sub(r"\(", r" ", row)
        row = re.sub(r"\)", r" ", row)
        row = re.sub(r",", r" ", row)
        row = re.sub(r"\*", r" ", row)
        row = re.sub(r"\{", r" ", row)
        row = re.sub(r"\}", r" ", row)
        row = re.sub(r";", r" ", row)
        row = re.sub(r"\[", r" ", row)
        row = re.sub(r"\]", r" ", row)
        row = re.sub(r"&", r" ", row)
        row = re.sub(r"=", r" ", row)
        row = re.sub(r"\+", r" ", row)
        row = re.sub(r"-", r" ", row)
        row = re.sub(r"%", r" ", row)
        row = re.sub(r"^", r" ", row)
        row = re.sub(r"\.", r" ", row)
        row = re.sub(r">", r" ", row)
        row = re.sub(r"<", r" ", row)
        row = re.sub(r"!", r" ", row)
        row = re.sub(r"~", r" ", row)
        row = re.sub(r":", r" ", row)
        row = re.sub(r"/", r" ", row)
        text += row + "\n"

    count = 0
    src = ""
    for w in text.split():
        for key in keyword:
            if w == key:
                w = " "

        src += w + " "

    for word in src.split():
        if word[0].isalpha():
            count += 1
            #print(word)

    return count


#end search_identificator()


def search_operator(source):
    """
    Metoda vraci pocet operatoru. Vstupni soubor je upraven pomoci regulernich vyrazu, Jsou odmazany
    pripady ukazale na char, int atd. A nasledne je text analyzovan automatem, ktery pocita regulerni vyrazy.
    @param source vstupni soubor
    @return count vraci pocet nalezenych identifikatoru
    """
    count = 0
    init = 0
    state = 0

    source = re.sub(r"(char\s*\*+)|(\*+\s*char)|(char\s*(\[+\s*\*\s*\])+)", r" ", source)
    source = re.sub(r"(int\s*\*+)|(\*+\s*int)|(int\s*(\[+\s*\*\s*\])+)", r" ", source)
    source = re.sub(r"(double\s*\*+)|(\*+\s*double)|(double\s*(\[+\s*\*\s*\])+)", r" ", source)
    source = re.sub(r"(float\s*\*+)|(\*+\s*float)|(float\s*(\[+\s*\*\s*\])+)", r" ", source)
    source = re.sub(r"(long\s*\*+)|(\*+\s*long)|(long\s*(\[+\s*\*\s*\])+)", r" ", source)
    source = re.sub(r"(short\s*\*+)|(\*+\s*short)|(short\s*(\[+\s*\*\s*\])+)", r" ", source)
    source = re.sub(r"(const\s*\*+)|(\*+\s*const)", r" ", source)

    source = re.sub(r"\(\s*\*\s*\)", r" ", source)
    source = re.sub(r"\*+\s*[a-zA-Z_]+", r"*a", source)
    source = re.sub(r"\*+\s*\(.*\)", r" ", source)
    source = re.sub(r"\*+\s*\)", r" ", source)
    source = re.sub(r"\}\s*\*+(\s*.*\*+)*", r" ", source)
    source = re.sub(r"\.\.+", r" ", source)
    source = re.sub(r"(\-|\+)?\d+\.\d+((e|E)(\-|\+)\d+)?", r" ", source)

    if re.search(r"[a-zA-Z_]+\.[a-zA-Z_]+", source):
        count += 1
        source = re.sub(r"[a-zA-Z_]+\.[a-zA-Z_]+", r" ", source)

    for char in source:
        if state == init:
            if char == '+' or char == '-' or char == '*' or char == '/' or char == '%' or char == '~' or char == '=':
                state = 1
            elif char == '!' or char == '^' or char == '<' or char == '>' or char == '&' or char == '|' or char == '.':
                state = 1
        elif state == 1:
            if char == '+' or char == '-' or char == '&' or char == '|':
                state = 2
            elif char == '=':
                state = 3
            elif char == '<' or char == '>':
                state = 4
            else:
                state = init
                count += 1
        elif state == 2:
            if char == '+' or char == '-' or char == '&' or char == '|':
                state = 1
                count += 1
            else:
                count += 1
                state = init
        elif state == 3:
            count += 1
            state = init
        elif state == 4:
            if char == '=':
                state = 3
            else:
                count += 1
                state = init
    return count


# end search_operator()

def extension_ind(source):
    state = 0
    src = ""
    count = 0
    bracket = 0

    for ch in source:
        if state == 0:
            if ch == '[':
                state = 1
                src += ch
                count += 1
            else:
                src += ch
        elif state == 1:
            if ch == ']':
                if bracket == 0:
                    src += ch
                    state = 0
                else:
                    bracket -= 1
            elif ch == '[':
                bracket += 1



    src = re.sub(r"\s*", r"",src)
    state = 0
    ch = ""

    counter = 0
    con = 0
    for ch in src:
        if state == 0:
            if ch == '*':
                state = 1
                counter += 1
                con += 1
        elif state == 1:
            if ch.isalpha():
                state = 2
            elif ch == '*':
                counter += 1
                con += 1
            else:
                counter -= con
        elif state == 2:
            if ch == '[':
                state = 0

    return count + counter

# end extension_ind()

def parse_file(file):
    """
    Metoda  nacita soubor a podle zadeneho parametru je soubor alalyzovan.
    @param file jmeno souboru
    @return count vraci hodnotu podle zvoleneho parametru
    """
    global g_pattern, g_param, g_s

    keyword = ["int", "long", "short", "double", "float", "signed", "unsigned", "char",
               "if", "else", "while", "do", "for", "case", "switch", "default", "break", "continue",
               "struct", "typedef", "sizeof", "union", "enum", "void", "static", "extern", "return",
               "register", "auto", "volatile", "const", "goto", "inline", "restrict", "_Bool", "_Complex", "_Imaginary"]

    try:
        with  codecs.open(file, 'r', 'iso-8859-2') as content_file:
            read_file = content_file.read()
            content_file.close()
    except IOError:
        sys.stderr.write("I/O error")
        sys.exit(21)

    text = re.sub(r'\r\n', r"\n", read_file)

    #rozsirenin COM
    if g_param == 'c' and g_s == 1:
        (count, text) = search_comment(text)
        return count

    # hledej patern
    if g_param == 'w':
        count = text.count(g_pattern)
        return count

    # spocitej komenate
    if g_param == 'c' and g_s == 0:
        (count, text) = search_comment(text)
        return count

    # upraveni souboru pro funkce k,o,i
    text = delete_string(text)
    text = delete_backslash(text)
    text = delete_macro2(text)
    (count, text) = search_comment(text)

    # hledej klicove slova
    if g_param == 'k':
        count = search_keyword(text, keyword)

    # hledej operatory
    if g_param == 'o':
        count = search_operator(text)
    # hledej identifikatory
    if g_param == 'i':
        count = search_identificator(text, keyword)

    # rozsireni IND
    if g_param == 'o' and g_s == 1:
        count = extension_ind(text)
        return count

    return count


# end parseFile()

def output():
    """
    Metoda pristupuje ke globalnim promennym, ktere budou následne zpracovány.
    Do globalni promme g_arraxStats jsou ukladany hodnoty pro konkretni soubor z g_arrayFile.
    Indexi obou promennych jsou navzajem shodne. metoda se stara o vystup podle zadane specifikace
    """
    global g_output, g_arrayFile, g_arrayStats, g_p
    celkem = 0
    out = []

    for filee in g_arrayFile:
        result = parse_file(filee)
        g_arrayStats.append(result)
        celkem += result

    delka_str = len("CELKEM:")
    delka_num = 0

    for i in range(len(g_arrayFile)):
        cesta = os.path.abspath(g_arrayFile[i])
        if g_p == 1:
            cesta = os.path.split(cesta)[1]

        if delka_str < len(cesta):
            delka_str = len(cesta)

        if delka_num < len(str(g_arrayStats[i])):
            delka_num = len(str(g_arrayStats[i]))

        g_arrayFile[i] = cesta

    rozsah = delka_num + delka_str + 1
    for i in range(len(g_arrayFile)):
        #print("%d" %i + ": %s" %g_arrayFile[i] + ":%d" % g_arrayStats[i])
        out.append("%s" % g_arrayFile[i] + " " * (
            rozsah - len(g_arrayFile[i]) - len(str(g_arrayStats[i]))) + "%d" % g_arrayStats[i])

    out.sort()
    out.append("CELKEM:" + " " * (rozsah - len("CELKEM:") - len(str(celkem))) + "%d" % celkem + "\n")

    if g_output == "":
        #print("tisknu na stdion")
        g_output = sys.stdout
        g_output.write("\n".join(out))
    else:
        #print("tisknu do souboru %s" % g_output)

        try:
            #with open(io.StringIO(g_output), 'w') as g_output:
            with open(g_output, 'w') as g_output:
                g_output.write("\n".join(out))
                g_output.close()
        except IOError as err:
            print("I/O error: {0}".format(err))
            sys.exit(3)


#end output()

def main():
    #nacteni parametru
    param()

    #ziskani vsech souboru
    analyze_input()

    # vystup souboru
    output()

    sys.exit(0)

# end main

if __name__ == "__main__":
    main()
